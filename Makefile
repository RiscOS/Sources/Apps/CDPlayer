# Copyright 1996 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for CDPlayer
#

COMPONENT  = CDPlayer
TARGET     = cdplayer
INSTTYPE   = app
APP_OBJS   = cdplayer
LIBS       = ${RLIB}
INSTAPP_FILES = !Boot !Help !Run !sprites cdplayer config sprites Templates

include CApp

# Dynamic dependencies:
